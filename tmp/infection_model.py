from time import time
from typing import Tuple, Dict
from sympy import *

from collections import namedtuple
from numpy.random import normal
import matplotlib.pyplot as plt

Infection_Update_State = namedtuple('Infection_Update_State',
                                    ["healthy", "newly_infected", "newly_recovered", "newly_dead"])


def recovery_chance_to_death_chance(recovery_chance):  # Todo: make not shitty
    return (1 - recovery_chance) / 50


def relativeNormal(c, div=100):
    return normal(c, c / div)


def infection_update(*, healthy_pop: int, infected_pop: int, resistance: float,
                     recovery_chance: float) -> Infection_Update_State:
    # Greater infected -> more chance to transmit
    # Less healthy -> less chance to transmit
    # Logistic?

    # y' = resistance * y * (1-y)/pop

    total_pop = healthy_pop + infected_pop
    d_infected: int = max(
        round(1 / relativeNormal(resistance, div=3) * infected_pop * (1 - infected_pop / total_pop)),
        0
    )
    if healthy_pop - d_infected < 0: d_infected = 0

    d_recovered: int = max(round(relativeNormal(recovery_chance) * infected_pop), 0)  # Simple linear model
    d_dead: int = max(round(relativeNormal(recovery_chance_to_death_chance(recovery_chance)) * infected_pop), 0)

    healthy: int = healthy_pop - d_infected

    return Infection_Update_State(
        healthy=healthy,
        newly_infected=d_infected,
        newly_dead=d_dead,
        newly_recovered=d_recovered
    )


if __name__ == '__main__':
    healthy = 100000
    infected = 500
    dead = 0
    recovered = 0
    resistance = 3.5
    recovery = 0.2

    X = []
    Y = []
    infected_escrow = [0] * 20  # Time delay (janky incubation period)
    start = time()
    for i in range(350):
        # resistance = 2.5 + 1 * i / 200
        if healthy <= 0: healthy = 0
        ret = infection_update(healthy_pop=healthy, infected_pop=infected, resistance=resistance,
                               recovery_chance=recovery)
        healthy = ret.healthy
        infected_escrow.append(ret.newly_infected)
        infected += infected_escrow.pop(0) - (ret.newly_dead + ret.newly_recovered)
        dead += ret.newly_dead
        recovered += ret.newly_recovered

        X.append(i)
        Y.append([healthy, infected, dead, recovered, healthy + recovered])
        # Y.append([healthy, infected])
    end = time()

    print(start - end)
    f = plt.figure()
    l1, l2, l3, l4, l5 = plt.plot(X, Y)
    plt.legend((l1, l2, l3, l4, l5), ('healthy', 'infected', 'dead', 'recovered (immune)', 'alive'))
    plt.xlabel('t')
    plt.ylabel('pop')
    plt.show()
    f.savefig("out.pdf")
