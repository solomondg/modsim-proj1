from typing import Dict

import pandas as pd
import numpy as np
import scipy as sp
import networkx as nx
from idiot import *

df = pd \
    .read_csv("data/flights_2017.csv") \
    .drop(['ORIGIN', 'ORIGIN_CITY_NAME', 'DEST', 'DEST_CITY_NAME', 'Unnamed: 7'], axis=1)
df.drop(df[df.ORIGIN_STATE_ABR == df.DEST_STATE_ABR].index, axis=0)

df = df[df['ORIGIN_STATE_ABR'].isin(states)]
df = df[df['DEST_STATE_ABR'].isin(states)]
df = df[~(df['ORIGIN_STATE_ABR'] == df['DEST_STATE_ABR'])]

with open('data/census-state-populations.csv', 'r') as f:
    state_populations = {k: v for k, v in [[us_state_abbrev[i.split(',')[0]], int(i.split(',')[1])] for i in f.read().split('\n')[0:-1]]}  # I apologize

# flight_count: Dict[str, Dict[str, int]] = dict.fromkeys(states, dict.fromkeys(states, 0))
# def add_fc(origin, dest):
#    flight_count[origin][dest] += 1
# df.apply(
#    lambda row: add_fc(row['ORIGIN_STATE_ABR'], row['DEST_STATE_ABR']),
#    axis=1
# )

state_positions = pd.read_csv("data/state_positions.csv")
state_positions.set_index("State", inplace=True)

G = nx.Graph()
for state in states:
    G.add_node(i,
               population=None,
               num_healthy=None,
               num_infected=0,
               infection_resistance=None,
               num_dead=0,
               pos=((state_positions.ix[state].Latitude, state_positions.ix[state].Longitude)))


for state in states:
    G[state]['population'] = state_populations[state]
    for j in states:
        G.add_edge(i, j, flights=0)


def add_fc(origin, dest):
    G[origin][dest]['flights'] += 1


df.apply(
    lambda row: add_fc(row['ORIGIN_STATE_ABR'], row['DEST_STATE_ABR']),
    axis=1
)
