from spatial_infection_model import simulation_controller, visualize

USA_SIM = simulation_controller.intialize()
USA_SIM.nodes_from_data("data/state_data.csv",
                        index = "abv",
                        population = "population",
                        num_susceptible = "population",
                        num_infected = 0,
                        num_exposed = 0,
                        num_dead = 0,
                        num_recovered = 0,
                        infection_resistance = 0,
                        posx = "longitude",
                        posy = "latitude",
                        )

USA_SIM.branches_from_data("data/neighbors-states.csv",
                            nodeA = "state1",
                            nodeB = "state2",
                            weight = 0
                            )
'''
for state in USA_SIM.states:
    USA_SIM.new_sim(seed_state = state, infected = 100)
    USA_SIM.run_simulation(steps = 365)  # Whole year
'''

#DISPLAY_USA_SIM = visualize.view_controller.initialize(country = "USA")
#DISPLAY_USA_SIM.



#maps, showfunc = visualize.generate_map()

#showfunc(maps)
