
def generate_map(network, pos_id = "pos"):
    from bokeh.sampledata import us_states, us_counties, unemployment
    from bokeh.plotting import figure, show, output_file
    from bokeh.io import show, output_notebook
    from bokeh.models import Plot, Range1d, MultiLine, Circle, HoverTool, StaticLayoutProvider
    from bokeh.models.graphs import from_networkx

    import networkx as nx
    us_states = us_states.data.copy()
    us_counties = us_counties.data.copy()
    unemployment = unemployment.data

    del us_states["HI"]
    del us_states["AK"]

    state_xs = [us_states[code]["lons"] for code in us_states]
    state_ys = [us_states[code]["lats"] for code in us_states]


    output_file("display/choropleth.html", title="choropleth.py example")

    p = figure(title="US Unemployment 2009", toolbar_location="left",
    plot_width=1100, plot_height=700)

    G = network.G

    fixed_layout = nx.get_node_attributes(G,'pos')
    graph_renderer = from_networkx(G, nx.spring_layout)

    graph_renderer.layout_provider.graph_layout

    fixed_layout_provider = StaticLayoutProvider(graph_layout=fixed_layout)
    graph_renderer.layout_provider = fixed_layout_provider

    #p.patches(county_xs, county_ys, fill_color=county_colors, fill_alpha=0.7,
    #    line_color="white", line_width=0.5)
    p.patches(state_xs, state_ys, fill_alpha=0.0,
    line_color="#884444", line_width=2)

    p.renderers.append(graph_renderer)

    return p, show


def healthiness_metric():
    import seaborn as sb
    import pandas as pd
    import matplotlib.pyplot as plt

    data = pd.read_csv("././data/state-healthiness.csv")
    data = data.sort_values("VALUE")
    fig = plt.figure(dpi = 150, figsize = (15, 6))
    sb.barplot(y="VALUE", x="abv", data = data)

    plt.title("Normalized healthiness metric for the USA")
    plt.xlabel("State")
    plt.ylabel("Healthiness metric")
