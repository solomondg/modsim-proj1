import numpy as np
import networkx as nx
import pandas as pd


def intialize():
    return simulation()


class simulation:
    """
    Contains DiGraph of nodes with attributes
    Each tick runs a specific update function on each node
    """
    def __init__(self):
        self.G = nx.Graph()

    def nodes_from_data(self,
                        data_file,
                        index = "abv",
                        population = "population",
                        num_infected = 0,
                        num_exposed = 0,
                        num_dead = 0,
                        num_recovered = 0,
                        infection_resistance = 0,
                        num_susceptible = "population",
                        posx = "longitude",
                        posy = "latitude",
                        **kwargs):

        data = pd.read_csv(data_file)
        data.set_index(index, inplace=True)
        for index, row in data.iterrows():
            self.G.add_node(index,
                            population = row[population],
                            num_susceptible = row[population],
                            num_exposed = num_exposed,
                            num_infected = num_infected,
                            num_recovered = num_recovered,
                            infection_resistance = infection_resistance,
                            num_dead = num_dead,
                            pos=(( row[posx], row[posy])),
                            **kwargs)

        print("Added nodes from: "+data_file)


    """
    Adds branches from loosely formatted csv data with required
    """
    def branches_from_data(self, \
                           data_file,
                           nodeA = "state1",
                           nodeB = "state2",
                           weight = "volume",
                           **kwargs):
        data = pd.read_csv(data_file)
        branches = 0
        for index, row in data.iterrows():
            if (self.G.has_node(row[nodeA]) and self.G.has_node(row[nodeB]) ):
                self.G.add_edge(row[nodeA], row[nodeB])
                branches+=1

        print("Added "+str(branches)+" branches from: "+data_file)

    def node_attributes_from_data(self,\
                                  data_file,
                                  metric_id = "VALUE",
                                  index_id = "abv",
                                  attribute_name = "metric"
                                 ):
        data = pd.read_csv(data_file)
        data.set_index(index_id, inplace = True)
        added = 0
        for index, row in data.iterrows():
            if index in self.G.nodes:
                self.G.nodes[index][attribute_name] = row[metric_id]
                added +=1

        print("Added metric for {} states.".format(added))



    def simulate(self,
                 node_update = None,
                 model_update = None,
                 overwrite_nodes = {},
                 steps = 365 ):
        sim_results = results()
        for modified_node, attributes in overwrite_nodes.items():
            for attribute, value in attributes.items():
                self.G.node[modified_node][attribute] = value
                #print("Modified node {} attribute {} with new value {}.".format(modified_node, attribute, value))

        for step in range(steps):
            if model_update != None:
                model_update(self)

            for node_id in self.G.nodes:
                node = self.G.nodes[node_id]
                node_update(node)
                sim_results.add_data(node_id, node, step)
        return sim_results




    def insert_data_into_nodes(self,
                               datafile,
                               index = "abv",
                               field = "" ):
        pass

    def insert_data_into_branches(self,
                                  datafile,
                                  nodeA = "state1",
                                  nodeB = "state2",
                                  field = "" ):
        pass


class results:
    def __init__(self):
        self.data = {'all':{}}
        pass

    def add_data(self, node_id, node_attributes, step = 0):
        newstep = False
        if 'step' in self.data:
            if self.data['step'][-1] != step:
                self.data['step'].append( self.data['step'][-1] + 1)
                newstep = True
        else:
            self.data['step'] = [0]

        # Make sure the node exists in the dataset
        if not node_id in self.data:
            self.data[node_id] = {}

        for field, value in node_attributes.items():
            if field in self.data[node_id]:
                self.data[node_id][field].append(value)
            else:
                self.data[node_id][field] = [value]

            # Make sure the field exists for the 'all' dataset
            if not field in self.data['all']:
                self.data['all'][field] = [value]

            # Either push current value to new step or increment existing value
            if newstep:
                self.data['all'][field].append(value)
            else:
                self.data['all'][field][-1] += value


    def plot_data(self, node_id = "ALL", data_attributes = [], plot_title = 'Data graphed', displayrange = [0, -1], plot_xlabel = None, plot_ylabel = None):
        from matplotlib.pyplot import figure, xlim, ylim, title, xlabel, ylabel, scatter, plot, legend, gca
        fig = figure(dpi = 150)
        data_x = self.data["step"][displayrange[0]:displayrange[1]]

        for y_index in data_attributes:
            data_y = self.data[node_id][y_index][displayrange[0]:displayrange[1]]
            scatter(data_x, data_y, s = 1, label = y_index)

        legend()


        title(', '.join(data_attributes))
        if plot_xlabel is None: plot_xlabel = "step"
        if plot_ylabel is None: plot_ylabel = 'count'
        gca().set_ylim(bottom=0)
        xlabel(plot_xlabel)
        ylabel(plot_ylabel)
        return fig


def transact_people(nodeA, nodeB, people):

    total_mobile = nodeA['num_susceptible'] + nodeA['num_exposed'] + nodeA['num_recovered']

    leaving_susceptible = (nodeA['num_susceptible'] / total_mobile) * people
    leaving_exposed = (nodeA['num_exposed'] / total_mobile) * people
    leaving_recovered = (nodeA['num_recovered'] / total_mobile) * people

    nodeA['num_susceptible'] -= leaving_susceptible
    nodeA['num_exposed'] -= leaving_exposed
    nodeA['num_recovered'] -= leaving_recovered

    nodeB['num_susceptible'] += leaving_susceptible
    nodeB['num_exposed'] += leaving_exposed
    nodeB['num_recovered'] += leaving_recovered
